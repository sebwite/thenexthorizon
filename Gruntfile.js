module.exports = function (grunt) {

  require('time-grunt')(grunt);
  require('load-grunt-tasks')(grunt, {
    pattern: ['grunt-*', '*' ,'!*postcss-reporter', '!*autoprefixer', '!time-grunt', '!load-grunt-tasks']
  });


  var targets = {
    dist: {name: 'dist', dest: '.'},
    dev : {name: 'dev', dest: '.'}
  };

  var _t = require('./grunt/targets')(grunt,  targets);
  //var ifTarget = _t.ifTarget;
  var setTarget = _t.setTarget;

  var config = {

    /**/
    //      Config
    /**/
    // Paths & Target Paths based on target option, defaults to dev
    // grunt task1 task2 build --target=dist
    src             : '.',
    bower_components: '<%= src %>/assets/bower_components',
    targets         : targets,
    target          : targets[grunt.option('target') || 'dev'],
    ifTarget: _t.ifTarget,


    /**/
    //      Styles
    /**/
    sass_globbing: {
      all: {
        options: {
          useSingleQuotes: true,
        },
        files: {
          '<%= src %>/assets/sass/_layouts.scss': '<%= target.dest %>/assets/sass/layouts/**/*.scss',
          '<%= src %>/assets/sass/_objects.scss': '<%= target.dest %>/assets/sass/objects/**/*.scss',
          '<%= src %>/assets/sass/_components.scss': '<%= target.dest %>/assets/sass/components/**/*.scss',
          '<%= src %>/assets/sass/_utilities.scss': '<%= target.dest %>/assets/sass/utilities/**/*.scss',
        },
      },
    },
    sass: {
      all: {
        options: {
          sourceMap: true,
          outputStyle: "<%= target.name === 'dist' ? 'compressed' : 'expanded' %>"
        },
        files: [{
          expand: true,
          cwd: '<%= src %>/assets/sass',
          src: ['!**/_*.scss', '**/*.scss'],
          dest: '<%= target.dest %>/assets/css',
          ext: '.css'
        }],
      },
    },
    postcss: {

      prefix: {
        options: {
          processors: [
            require('autoprefixer')({ browsers: 'last 3 versions'}), // add vendor prefixes
          ]
        },
        src: '<%= target.dest %>/assets/css/**/*.css'
      },
      //lint: {
      //    options: {
      //        processors: [
      //            require("stylelint")({ /* your options */ }),
      //            require("postcss-reporter")({ clearMessages: true })
      //        ]
      //    },
      //    // lint all the .css files in the css directory
      //    src: '<%= target.dest %>/assets/css/stylelint.css'
      //}

    },
    sasslint: {
      options: {
        configFile: 'grunt/.sass-lint.yml',
        outputFile: 'sasslint.log'
      },
      target: ['<%= src %>/assets/sass/**/*.scss']
    },


    /**/
    //      Documentation
    /**/
    sassdoc: {
      all: {
        src: '<%= src %>/assets/sass/**/*.scss',
      },
    },


    /**/
    //      Server
    /**/
    browserSync: {
      bsFiles: {
        src: ['<%= target.dest %>/assets/css/**/*.css', '<%= src %>/**/*.html']
      },
      options: {
        server: {
          baseDir: "./"
        },
        browser: "chrome",
        watchTask: true
      }
      // options: {
      //   //proxy: "http://spotonvision.dev",
      //   //watchTask: true,
      //   //browser: "chrome",
      // }
    },


    /**/
    //      Watcher
    /**/
    watch: {
      options: {
        livereload: true,
        spawn: false
      },
      sass: {
        files: ['<%= src %>/assets/sass/**/*.scss'],
        tasks: ['sass_globbing:all', 'sass:all']
      },
    },
  };
  grunt.initConfig(config);

  // Load Grunt tasks
  [
    ['build', 'Build Sass', [
      'sass_globbing:all',
      'sass:all'
    ]],
    ['dist', 'Build new dist', [
      'target:dist',
      'build',
      'postcss:prefix',
      'sassdoc:all'
    ]]
  ].forEach(function(item){
    grunt.registerTask(item[0], item[1], item[2]);
  });

  grunt.task.registerTask('target', 'Set target trough task', function(name){ setTarget(name) });

  grunt.registerTask('watcher', ['build', 'browserSync', 'watch']);
};
