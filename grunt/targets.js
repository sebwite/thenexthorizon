module.exports = function (grunt, targets) {


    function setTarget(name) {
        grunt.config.set('target', grunt.config.get('targets.' + name));
    }

    function ifTarget(name, then, els) {
        els = typeof(els) === 'undefined' ? false : els;


        grunt.log.ok(grunt.config.get("target.name"));
        return grunt.config.get("target.name") === name ? then : els;
    }

    return {
        setTarget: setTarget,
        ifTarget : ifTarget
    }
};
